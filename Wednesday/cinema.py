import mysql.connector as mysql


def create_structure():
    conn = mysql.connect(host="localhost", user="root", password="Frectie!234")
    with conn.cursor() as c:
        c.execute('CREATE DATABASE IF NOT EXISTS cinema')
        c.execute('''
        CREATE TABLE IF NOT EXISTS cinema.clienti(
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        nume VARCHAR(25) NOT NULL,
        email VARCHAR(30) NOT NULL,
        telefon VARCHAR(20) NOT NULL,
        data_nasterii VARCHAR(30) NOT NULL);''')

        c.execute('''
        CREATE TABLE IF NOT EXISTS cinema.filme(
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        nume VARCHAR(25) NOT NULL,
        an_lansare INT,
        descriere TEXT NOT NULL);''')

        c.execute('''
        CREATE TABLE IF NOT EXISTS cinema.sali(
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        denumire TEXT NOT NULL,
        capacitate INT NOT NULL,
        vip BOOLEAN NOT NULL DEFAULT 0);''')

        c.execute('''CREATE TABLE IF NOT EXISTS cinema.bilete(
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        film_id INT,
        sala_id INT,
        client_id INT,
        pret INT,
        CONSTRAINT fk_film_id FOREIGN KEY (film_id) REFERENCES filme(id),
        CONSTRAINT fk_sala_id FOREIGN KEY (sala_id) REFERENCES sali(id),
        CONSTRAINT fk_client_id FOREIGN KEY (client_id) REFERENCES clienti(id));''')
    conn.close()


create_structure()
conn = mysql.connect(host="localhost", user="root", password="Frectie!234", database='cinema')


def populare1():
    with conn.cursor() as c:
        c.execute(f'''INSERT INTO clienti (nume, email, telefon, data_nasterii)
                     VALUES ('Alex', 'mail1', '072', '12.12.2021') ;''')
        conn.commit()


def populare2():
    with conn.cursor() as c:
        c.execute('''INSERT INTO filme (nume, an_lansare, descriere) 
                    VALUES ('Hulk', '12.12.2016', 'Omu verde');''')
        c.execute('''INSERT INTO sali (denumire, capacitate, vip) 
                    VALUES ('Sala 1', 100, True);''')
        conn.commit()


def populare3():
    with conn.cursor() as c:
        c.execute('''INSERT INTO bilete (film_id, sala_id, client_id, pret) 
                    VALUES (1, 1, 1, 60)''')
        conn.commit()


populare1()
populare2()


# populare3()

def calculare_pret_per_an():
    year = int(input('Enter movie year: '))
    with conn.cursor() as c:
        c.execute('SELECT SUM(pret) from bilete where film_id=1 and YEAR(data)=%s;', (year,))
        result = c.fetchone()
        print(f'Incasare de {result[0]}')


def SUM_MIN():
    year = int(input('Enter movie year: '))
    with conn.cursor() as c:
        c.execute('SELECT SUM(pret) from bilete where film_id=1 and YEAR(data)=%s;', (year,))
        result = conn.fetchone()
        print(result)


SUM_MIN()
