import mysql.connector as mysql
from dateutil.parser import parse
from datetime import datetime


def create_structure():
    conn = mysql.connect(
        host='localhost',
        user='root',
        password='Frectie!234',
        database='')

    with conn.cursor() as c:
        c.execute('CREATE DATABASE IF NOT EXISTS trainers ')
        c.execute('''
        CREATE TABLE IF NOT EXISTS trainers.trainer(
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        name VARCHAR(25) NOT NULL,
        email VARCHAR(30) NOT NULL,
        location VARCHAR(20) NOT NULL,
        contract VARCHAR(40) NOT NULL,
        password TEXT NOT NULL);''')
        #
        c.execute('''CREATE TABLE IF NOT EXISTS trainers.session(
        id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        number_of_hours INT NOT NULL,
        start_date TIMESTAMP NOT NULL,
        stop_date TIMESTAMP NOT NULL,
        order_number TEXT NOT NULL,
        base_price DECIMAL(10.0) NOT NULL,
        group_id TEXT NOT NULL,
        is_paid BOOLEAN NOT NULL DEFAULT 0,
        trainer_id INT NOT NULL,
        CONSTRAINT fk_trainer_id FOREIGN KEY (trainer_id) REFERENCES trainer(id));''')
    conn.close()


def show_menu1():
    print('1.Login')
    print('2.Register')
    print('3.Exit')


def show_menu2():
    print('1.Add new training session')
    print('2.Get upcoming training sessions')
    print('3.Get unpaid training sessions')
    print('4.Generate a bill')
    print('5.Get total paid sum in a given year')
    print('6.Logout')
    print('7.Exit')


create_structure()

conn = mysql.connect(
    host='localhost',
    user='root',
    password='Frectie!234',
    database='')
user_logged = None

while True:
    if user_logged is None:
        show_menu1()
        option = int(input(f'Enter your option: '))

        if option == 3:
            break
        elif option == 1:
            email = input(f'Enter your email : ')
            password = input(f'Enter your password : ')
            with conn.cursor() as c:
                c.execute(f"SELECT id FROM trainers.trainer WHERE email='{email}' AND password='{password}'")
                result = c.fetchone()  # returns a single register or none
                print(result)
                if result:
                    user_logged = result[0]
                else:
                    print('Email or password is wrong!')
        elif option == 2:
            name = input(f'Enter your name : ')
            email = input(f'Enter your email : ')
            location = input(f'Enter your location : ')
            contract = input(f'Enter your contract number : ')
            password = input(f'Enter your password : ')
            with conn.cursor() as c:
                c.execute(
                    'INSERT INTO trainers.trainer (name, email, location, contract, password) VALUES (%s,%s,%s,%s,%s);',
                    (name, email, location, contract, password))
                conn.commit()
    else:
        show_menu2()
        option = int(input(f'Enter your option: '))
        if option == 7:
            break
        elif option == 6:
            user_logged = None
        elif option == 1:
            number_of_hours = input(f'Enter the number of hours : ')
            start_date = parse(input(f'Enter start date : '))
            stop_date = parse(input(f'Enter stop date : '))
            order_no = input(f'Enter order number : ')
            base_price = float(input(f'Enter the price : '))
            group_id = input(f'Enter the group id : ')
            with conn.cursor() as c:
                c.execute('INSERT INTO session (number_of_hours, start_date, stop_date, order_no, base_price, group_id,trainer_id) VALUES (%s,%s,%s,%s,%s,%s,%s);',(number_of_hours, start_date, stop_date, order_no, base_price, group_id, user_logged,))
            conn.commit()

        elif option == 2:
            with conn.cursor() as c:
                c.execute('SELECT * FROM session WHERE start_date> %s AND trainer_id = %s;',
                          (datetime.now(), user_logged))
                results = c.fetchall()
                for result in results:
                    print(result)
        elif option == 3:
            with conn.cursor() as c:
                c.execute('SELECT * FROM session WHERE is_paid=0 AND trainer_id=%s;', (user_logged,))
                result = c.fetchall()
                for result in results:
                    print(f'Your unpaid sessions are: ID ->{result[0]} Number of Hours->{result[1]} '
                          f'Start day->{result[2]} End day->{result[3]} Order no.->{result[4]} '
                          f'Base price->{result[5]} Group Id->{result[6]} Is Paid->{result[7]} '
                          f'Trainer Id->{result[8]}')
        elif option == 3:
            with conn.cursor() as c:
                c.execute()
        elif option == 3:
            with conn.cursor() as c:
                c.execute()
