import mysql.connector as mysql
import datetime


def create_structure():
    conn = mysql.connect(
        host="localhost",
        user="root",
        password="Frectie!234",
        database="",
    )

    with conn.cursor() as c:
        c.execute("CREATE DATABASE IF NOT EXISTS shop;")
        c.execute("""
            CREATE TABLE IF NOT EXISTS shop.utilizator (
                id INT PRIMARY KEY AUTO_INCREMENT,
                nume TEXT NOT NULL,
                email TEXT NOT NULL,
                parola TEXT NOT NULL
            );
        """)
        c.execute("""
            CREATE TABLE IF NOT EXISTS shop.produs (
                id INT PRIMARY KEY AUTO_INCREMENT,
                denumire TEXT NOT NULL,
                cantitate INT NOT NULL,
                pret DECIMAL(8, 2) NOT NULL 
            );
        """)
        c.execute("""
            CREATE TABLE IF NOT EXISTS shop.istoric (
                id INT PRIMARY KEY AUTO_INCREMENT,
                user_id INT NOT NULL,
                produs_id INT NOT NULL,
                data TIMESTAMP NOT NULL,
                cantitatea_cumparata INT NOT NULL,
                CONSTRAINT fk_user_id FOREIGN KEY(user_id) REFERENCES shop.utilizator(id),
                CONSTRAINT fk_produs_id FOREIGN KEY(produs_id) REFERENCES shop.produs(id)
            );
        """)

    conn.close()


create_structure()

conn = mysql.connect(
    host='localhost',
    user='root',
    password='Frectie!234',
    database='shop')

user_logged = None


def show_menu_1():
    print("1. Login")
    print("2. Register")
    print("0. Exit application")


def show_menu_2():
    print("1. Show product list")
    print("2. Buy product")
    print("3. Show history")
    print("4. Logout")
    print("0. Exit application")


def Login():
    email = input(f'Enter your email : ')
    password = input(f'Enter your password : ')
    with conn.cursor() as c:
        c.execute(f"SELECT id FROM utilizator WHERE email='{email}' AND parola='{password}'")
        result = c.fetchone()
        if result:
            x = result[0]
        else:
            print('Email or password is wrong!')
    return x


def Register():
    name = input(f'Enter your name : ')
    email = input(f'Enter your email : ')
    password = input(f'Enter your password : ')
    with conn.cursor() as c:
        c.execute(f'INSERT INTO shop.utilizator (nume, email,parola) VALUES (%s,%s,%s);', (name, email, password))
        conn.commit()


while True:
    if user_logged is None:
        show_menu_1()
        option = int(input(f'Enter your option: '))
        if option == 0:
            break
        elif option == 1:
            user_logged = Login()
        elif option == 2:
            Register()
    else:
        show_menu_2()
        option = int(input(f'Enter your option: '))
        if option == 0:
            break
